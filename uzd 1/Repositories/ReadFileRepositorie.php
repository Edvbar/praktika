<?php
namespace Repositories;

/**
 * @package Controllers
 * @author Edvinas Baranauskas
 * @version 1.0
 * @access public
 */
class ReadFileRepositorie
{
    /**
    * @var array $errors holds error messages if any happends
    * @access private
    * @var array $data the data from a read file
    * @access private
    */
    private static $errors = array();
    private static $data;
    
    /**
     * adds a specific message to $errors array
     *
     * @param  array $message
     * @return void
     * @access private
     */
    private static function addError($message)
    {
        self::$errors = array_merge(self::$errors, (array)$message);
    }
    
    /**
     * reads a file based on files format
     *
     * @param  string $fileName
     * @param  string $upload
     * @return array
     * @access public
     */
    public static function readFile($fileName, $upload)
    {
        switch (ValidationRepository::getFormat($fileName)) {
            case 'json':
                $ret = ConversionRepository::fromJson(isset($upload) ? $upload : $fileName);
                if (gettype($ret) != 'array') self::addError($ret);
                else self::$data = $ret;
                break;
            case 'xml':
                $ret = ConversionRepository::fromXml(isset($upload) ? $upload : $fileName);
                if (gettype($ret) != 'array') self::addError($ret);
                else self::$data = $ret;
                break;
            case 'csv':
                $ret = ConversionRepository::fromCsv(isset($upload) ? $upload : $fileName);
                if (gettype($ret) != 'array') self::addError($ret);
                else self::$data = $ret;
                break;
            default:
                self::addError("Method not found for such file format");
        }
        if (empty(self::$errors)) return self::$data;
        return self::$errors;
    }
    
    /**
     * reads files safely - first validates then reads it
     *
     * @param  file $file
     * @param  array $availableFormats
     * @param  int $minSize
     * @param  int $maxSize
     * @return array
     * @access public
     */
    public static function readFileSafely($file, $availableFormats, $minSize, $maxSize)
    {
        self::addError(Validationrepository::validateFile($file, $availableFormats, $minSize, $maxSize));
        if (empty(self::$errors)) self::readFile($file['name'], isset($file['tmp_name']) ? $file['tmp_name'] : null);
        if (empty(self::$errors)) return array('data' => self::$data);
        return array('errors' => self::$errors);
    }
}
?>