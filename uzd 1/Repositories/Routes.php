<?php
namespace Repositories;

use Controllers\IndexController;

/**
* executes a Route based on url
* passes page name and function to create View
*/
Route::set('index.php', function() {
    IndexController::CreateView('IndexView');
});
?>