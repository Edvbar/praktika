<?php
namespace Repositories;

/**
 * @package Repositories
 * @author Edvinas Baranauskas
 * @version 1.0
 * @access public
 */
class ConversionRepository
{    
    /**
     * reading json format file into an array
     *
     * @param  string $fileName
     * @return mixed
     * @access public
     */
    public static function fromJson($fileName)
    {
        try {
            return json_decode(file_get_contents($fileName), true);
        } catch (Exception $e) {
            return "Unable to read json file";
        }
    }
    
    /**
     * reading xml format file into an array
     *
     * @param  string $fileName
     * @return mixed
     * @access public
     */
    public static function fromXml($fileName)
    {
        try {
            return json_decode(json_encode(simplexml_load_string(file_get_contents($fileName))), true);
        } catch (Exception $e) {
            return "Unable to read xml file";
        }
    }
    
    /**
     * reading csv format file into an array
     *
     * @param  string $fileName
     * @return mixed
     * @access public
     */
    public static function fromCsv($fileName)
    {
        try {
            $data = array_map('str_getcsv', file($fileName));
            array_walk($data, function(&$a) use ($data) {
                $a = array_combine($data[0], $a);
            });
            array_shift($data);
            return $data;
        } catch (Exception $e) {
            return "Unable to read csv file";
        }
    }
}
?>