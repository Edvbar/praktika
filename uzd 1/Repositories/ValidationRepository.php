<?php
namespace Repositories;

/**
 * @package Repositories
 * @author Edvinas Baranauskas
 * @version 1.0
 * @access public
 */
class ValidationRepository
{
    /**
    * @var array $errors holds error messages if any happends
    * @access private
    */
    private static $errors = array();
    
    /**
     * the start of validation
     * checks if provided file is single
     * executes remaining validation methods
     *
     * @param  file $file
     * @param  array $availableFormats
     * @param  int $minSize
     * @param  int $maxSize
     * @return array
     * @access public
     */
    public static function validateFile($file, $availableFormats, $minSize, $maxSize)
    {
        if (is_array($file['error'])) self::addError('Invalid parameters');
        else {
            self::uploadErrors($file);
            if(empty(self::$errors)) {
                self::checkSize($file, $minSize, $maxSize);
                self::checkFormat($file, $availableFormats);
            }
        }
        return self::$errors;
    }
    
    /**
     * checks if the uploaded files format is allowed (from settings.json)
     *
     * @param  file $file
     * @param  array $availableFormats
     * @return void
     * @access private
     */
    private static function checkFormat($file, $availableFormats)
    {
        if (!isset($availableFormats) || !in_array(self::getFormat($file['name']), $availableFormats))
            self::addError("Such format is NOT allowed");
    }
    
    /**
     * gets the file format
     *
     * @param  string $fileName
     * @return string
     * @access public
     */
    public static function getFormat($fileName)
    {
        $arr = explode(".", $fileName);
        return $arr[sizeof($arr) > 0 ? sizeof($arr)-1 : "INVALID"];
    }
    
    /**
     * checks if there where any upload errors
     *
     * @param  file $file
     * @return void
     * @access private
     */
    private static function uploadErrors($file)
    {
        switch ($file['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                self::addError('No file sent');
                break;
            default:
                self::addError('Unknown errors');
        }
    }
    
    /**
     * checks if the file size does not exceed the limits declared in settings (settings.json)
     *
     * @param  file $file
     * @param  int $minSize
     * @param  int $maxSize
     * @return void
     * @access private
     */
    private static function checkSize($file, $minSize, $maxSize)
    {
        if (isset($minSize)) 
            if ($file['size'] < $minSize) 
                self::addError('Filesize is too small');
        if (isset($maxSize)) 
            if ($file['size'] > $maxSize) 
                self::addError('Exceeded filesize limit');
    }
    
    /**
     * adds a specific message to $errors array
     *
     * @param  array $message
     * @return void
     * @access private
     */
    private static function addError($message)
    {
        self::$errors = array_merge(self::$errors, (array)$message);
    }
}
?>