<?php
namespace Repositories;

/**
 * @package Repositories
 * @author Edvinas Baranauskas
 * @version 1.0
 * @access public
 */
class Route
{    
    /**
     * sets a route and executes a chosen function
     *
     * @param  string $route
     * @param  function $function
     * @return void
     * @access public
     */
    public static function set($route, $function)
    {
        if ($_GET['url'] == $route) $function->__invoke();
    }
}
?>