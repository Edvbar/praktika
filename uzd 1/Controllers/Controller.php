<?php
namespace Controllers;

use Repositories\ReadFileRepositorie;

/**
 * @package Controllers
 * @author Edvinas Baranauskas
 * @version 1.0
 * @access public
 */
class Controller
{
    /**
    * @var array $data the data from a read file
    * @access protected
    */
    protected static $data;
    
    /**
     * gets data from settings files
     *
     * @return void
     * @access protected
     */
    final protected static function readSettings()
    {
        $path = 'settings.json';
        self::$data = ReadFileRepositorie::readFile($path, null);
    }
}
?>