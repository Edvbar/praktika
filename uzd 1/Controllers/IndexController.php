<?php
namespace Controllers;

use ViewModels\IndexViewModel;
use Repositories\ReadFileRepositorie;

/**
 * @package Controllers
 * @author Edvinas Baranauskas
 * @version 1.0
 * @access public
 */
class IndexController extends Controller
{
    /**
    * @var array $errors holds error messages if any happends
    * @access public
    */
    public static $errors = array();
    
    /**
     * Creates a View for index.php
     *
     * @param  string $viewName
     * @return void
     * @access public
     */
    public static function CreateView($viewName)
    {
        self::readSettings();
        /**
        * creates  ViewModel for a page
        */
        $view = new IndexViewModel();
        /**
        * transfer the data from array $data if any exist
        */
        if (!empty(self::$data)) {
            $view->pageHeader = array_key_exists('title', self::$data) ? self::$data['title'] : null;
            $view->formats = array_key_exists('formats', self::$data) ? self::$data['formats'] : null;
            $view->devName = array_key_exists('written_by', self::$data) ? self::$data['written_by'] : null;
            $view->checkSize = array_key_exists('check_size', self::$data) ? gettype(self::$data['check_size']) == 'boolean' ? self::$data['check_size'] : false : false;
            $view->minSize = array_key_exists('min_size', self::$data) ? self::$data['min_size'] : 0;
            $view->maxSize = array_key_exists('max_size', self::$data) ? self::$data['max_size'] : null;
        }
        /**
        * checks if any file was uploaded
        * if it was - validates it reads it
        */
        if (isset($_POST['submitFile'])) {
            if (isset($_FILES['uploadFile']['error'])) {
                $data = ReadFileRepositorie::readFileSafely($_FILES['uploadFile'], $view->formats, $view->minSize, $view->maxSize);
                if (array_key_exists('errors', $data)) self::addError($data['errors']);
                elseif (array_key_exists('data', $data)) $view->arr = $data['data'];
            }
            else self::addError('Invalid parameters');
        }
        /**
        * passes error messages, if any accured, to view model
        */
        $view->issues = self::$errors;
        require_once("./Views/$viewName.php");
    }
    
    /**
     * adds a specific message to $errors array
     *
     * @param  array $message
     * @return void
     * @access private
     */
    private static function addError($message)
    {
        self::$errors = array_merge(self::$errors, (array)$message);
    }
}
?>