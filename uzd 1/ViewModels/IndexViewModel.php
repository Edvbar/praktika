<?php
namespace ViewModels;

/**
 * @package ViewModels
 * @author Edvinas Baranauskas
 * @version 1.0
 * @access public
 */
class IndexViewModel
{
    /**
    * @var string $pageHeader for page header
    * @var boolean $checkSize if size validation is required (from settings)
    * @var int $minSize minimum size file that can be uploaded (from settings)
    * @var int $maxSize maximum filze size that can be uplaoded (from settings)
    * @var array $formats available upload formats (from settings)
    * @var string $devName devs name
    * @var array $issues list of issues that accured during the process
    * @var array $arr data from the uploaded file
    * @access public
    */
    public $pageHeader;
    public $checkSize;
    public $minSize;
    public $maxSize;
    public $formats;
    public $devName;
    public $issues;
    public $arr;
}
?>