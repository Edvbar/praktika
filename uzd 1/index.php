<?php
    /**
     * autoloader
     */
    spl_autoload_register(function($className) 
    {
        include dirname(__FILE__) . '/' . str_replace('\\', "/", $className) . '.php';
    });
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Praktika</title>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
</head>
<body>
    <?php
        require_once('Repositories/Routes.php');
    ?>
</body>
</html>