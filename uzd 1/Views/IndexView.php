<div id="page" class="grid">
    <?php
    require_once('./Views/Shared/Header.php');
    ?>
    <main id="main">
        <div class="arrayDisplay">
            <?php
            if (!empty($view->arr)) {
            ?>
                <div class="header">
                    Data inside the file:
                </div>
            <?php
                print_r($view->arr);
            }
            ?>
        </div>
    </main>
    <aside id="aside">
        <div class="h100 w100">
            <div class="header section">Information</div>
            <?php
            if (!empty($view->formats)) {
            ?>
            <div class="container section">
                <span class="title">File Formats</span>
                <div class="description">
                    <ul>
                        <?php
                        foreach ($view->formats as $format) {
                        ?> 
                        <li><?php echo $format; ?></li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <?php
            }
            if ($view->checkSize && !(!isset($view->maxSize) && $view->minSize <= 0)) {
            ?>
            <div class="container section">
                <span class="title">File size limit</span>
                <div class="description">
                    <ul>
                        <?php
                        if ($view->minSize > 0) {
                        ?>
                            <li>min: <?php echo $view->minSize; echo $view->minSize == 1 ? " byte" : " bytes"; ?> </li>
                        <?php
                        }
                        if (isset($view->maxSize)) {
                        ?>
                            <li>max: <?php echo $view->maxSize; echo $view->maxSize == 1 ? " byte" : " bytes"; ?></li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <?php
            }
            ?>
            <div class="container section">
                <div class="description">
                    <form enctype="multipart/form-data" action="<?php $_PHP_SELF; ?>" method="post" >
                        <input type="file" value="df" name="uploadFile" id="uploadFile">
                        <button type="submit" name="submitFile" class="w100">Submit</button>
                    </form>
                </div>
            </div>
            <?php
            if (!empty($view->issues)) {
            ?>
            <div class="container section">
                <span class="title">Issues</span>
                <div class="description">
                    <ul>
                        <?php
                        foreach ($view->issues as $issue) {
                        ?>
                        <li><?php echo $issue; ?></li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
    </aside>
    <?php
    require_once('./Views/Shared/Footer.php');
    ?>
</div>